<?php
use Illuminate\Support\Facades\Auth;

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('login');
});

Route::get('/', 'MainController@redirectPage');
Route::get('game', 'MainController@redirectPage');
Route::get('users', 'MainController@redirectPage');
Route::get('battle', 'MainController@redirectPage');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::post('first', 'MainController@firstRequest');
    Route::post('get-users', 'MainController@getUsers');
    Route::post('invite', 'MainController@invite');
    Route::post('cancel-outgoing-invite', 'MainController@cancelOutgoingInvite');
    Route::post('cancel-incoming-invite', 'MainController@cancelIncomingInvite');
    Route::post('confirm-invite', 'MainController@confirmlInvite');

    Route::group(['middleware' => ['playerGame']], function () {
        Route::post('restart-level', 'MainController@restartLevel');
        Route::post('exit-game', 'MainController@exitGame');

        Route::group(['middleware' => ['checkPlayers']], function () {
            Route::group(['middleware' => ['playerFree']], function () {
                Route::post('fight', 'MainController@fight');
                Route::post('weapon-equip', 'MainController@weaponEquip');
            });

            Route::group(['middleware' => ['userAction']], function () {
                Route::post('move', 'MainController@move')->middleware('playerFree');
                Route::post('attack', 'MainController@attack')->middleware('playerBattle');
            });
        });
    });
});