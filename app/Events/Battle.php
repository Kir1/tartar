<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Battle implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userChannelId;

    public $userId;

    public $battle;

    /**
     * Create a new event instance.
     *
     * @param $orders
     */
    public function __construct($userChannelId, $userId, $battle = true)
    {
     Log::info($battle);
        $this->userChannelId = $userChannelId;
        $this->userId = $userId;
        $this->battle = $battle;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('user.' . $this->userChannelId);
    }
}
