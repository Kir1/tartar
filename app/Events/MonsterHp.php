<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MonsterHp implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userChannelId;
    public $zoneIndex;
    public $monsterId;
    public $hp;

    /**
     * Create a new event instance.
     *
     * @param $orders
     */
    public function __construct($userChannelId, $zoneIndex, $monsterId, $hp)
    {
        $this->userChannelId = $userChannelId;
        $this->zoneIndex = $zoneIndex;
        $this->monsterId = $monsterId;
        $this->hp = $hp;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('user.' . $this->userChannelId);
    }
}
