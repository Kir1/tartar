<?php

namespace App\Http\Middleware;

use App\Models\Game;
use App\Game\GameManager;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPlayers
{
    public function handle($request, Closure $next)
    {
        if (GameManager::$currentUser->id != GameManager::$user->id) return GameManager::postError('Сейчас не твой ход.');
        if (GameManager::$currentUser->hp <= 0) return GameManager::postError('Ты мёртв.');
        if (GameManager::$nextUser->hp <= 0) GameManager::$nextUser = GameManager::$currentUser;
        return $next($request);
    }
}