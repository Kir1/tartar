<?php

namespace App\Http\Middleware;

use App\Game\GameManager;
use Closure;
use Illuminate\Support\Facades\Auth;

class PlayerFree
{
    public function handle($request, Closure $next)
    {
        $user = GameManager::$user;
        if ($user->battle) return GameManager::postError('Вы в бою и не можете совершить это действие.');
        return $next($request);
    }
}