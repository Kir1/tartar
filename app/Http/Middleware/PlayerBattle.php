<?php

namespace App\Http\Middleware;

use App\Game\GameManager;
use Closure;
use Illuminate\Support\Facades\Auth;

class PlayerBattle
{
    public function handle($request, Closure $next)
    {
        $user = GameManager::$user;
        if ($user->battle) return $next($request);
        return GameManager::postError('Вы не в бою.');
    }
}