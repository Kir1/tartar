<?php

namespace App\Http\Middleware;

use App\Models\Game;
use App\Game\GameManager;
use Closure;
use Illuminate\Support\Facades\Auth;

class PlayerGame
{
    public function handle($request, Closure $next)
    {
        GameManager::$user = Auth::user();
        GameManager::$userId = GameManager::$user->id;
        GameManager::getGame($request->gameId);
        if (GameManager::$game == null) return GameManager::postError('Такой игры нет.');
        GameManager::$gameUsers = GameManager::getCurrAndNextUsers(GameManager::$game);
        GameManager::$currentUser = GameManager::$gameUsers['currentUser'];
        GameManager::$nextUser = GameManager::$gameUsers['nextUser'];
        if (GameManager::$currentUser->id == GameManager::$user->id || GameManager::$nextUser->id == GameManager::$user->id) return $next($request);
        return GameManager::postError('Ты не участник игры');
    }
}