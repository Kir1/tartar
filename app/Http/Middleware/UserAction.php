<?php

namespace App\Http\Middleware;

use App\Events\EndGame;
use App\Events\ModalMessage;
use App\Events\PlayerMove;
use App\Events\ReductionMoves;
use App\Models\Game;
use App\Game\GameManager;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserAction
{
    public function handle($request, Closure $next)
    {
        $game = GameManager::$game;

        $countMoves = $game->count_moves - 1;

        //
        $response = $next($request);
        ///

        $game = GameManager::$game;
        if ($game == null) return \Response::json('');

        ///уровень пройден
        $countMontsers = $game->monsters->count();
        $countNotes = $game->notes;
        if ($countMontsers == 0 && $countNotes == 0) {
            ///победа -  кончились все уровни
            if ($game->level == GameManager::$countLevels) {
                $usersForPlayers = GameManager::getFreeUsersForPlayers($game->player1_id, $game->player2_id);
                event(new EndGame($game->player1_id, 'Победа', 'Поздравляю! Вы с напарником прошли игру!', $usersForPlayers['usersForUser1']));
                event(new EndGame($game->player2_id, 'Победа', 'Поздравляю! Вы с напарником прошли игру!', $usersForPlayers['usersForUser2']));
            } //иначе переход на след уровень
            else {
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ModalMessage($gameUser->id, 'Новый уровень', 'Поздравляю! Вы с напарником перешли на следующий уровень.'));
                };
                GameManager::pushLevel($game->user1->id, $game->user2->id, $game->level + 1);
            }

            $game->delete();

            return \Response::json('');
        }
        //////////

        ///проигрышь - рестарт
        $movesEnd = ($countMoves == 0);
        $usersDeath = ($game->user1->hp == 0 && $game->user2->hp == 0);
        if ($movesEnd || $usersDeath) {
            GameManager::reloadLevel($game->id);
            $message = '';
            if ($movesEnd) $message = 'Закончились игровые ходы.';
            elseif ($usersDeath) $message = 'Все игроки погибли';
            foreach (GameManager::$gameUsers as $gameUser) {
                event(new ModalMessage($gameUser->id, 'Перезагрузка уровня', $message));
            };
            return \Response::json('');
        }

        //ничего не изменилось, минус ход и даём ход следующему игроку
        $game->move_player_id = GameManager::$nextUser->id;
        $game->count_moves = $countMoves;
        $game->save();

        foreach (GameManager::$gameUsers as $gameUser) {
            event(new ReductionMoves($gameUser->id));
        };

        event(new PlayerMove($game->move_player_id));

        return $response;
    }
}