<?php

namespace App\Http\Controllers;

use App\Events\Battle;
use App\Events\ChatMessage;
use App\Events\ClearZone;
use App\Events\EndGame;
use App\Events\GoToZone;
use App\Events\ModalMessage;
use App\Events\MonsterHp;
use App\Events\UserHp;
use App\Game\GameManager;
use App\Models\Game;
use App\Models\Invite as InviteModel;
use App\Models\Monster;
use App\Models\User;
use App\Models\Weapon;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\IncomingInvite as IncomingInvite;
use App\Events\CancelOutgoingInvite as CancelOutgoingInvite;

class MainController extends Controller
{
    public function redirectPage()
    {
        $user = Auth::user();
        if ($user) return view('welcome', ['userId' => $user->id]);
        return redirect()->route('login');
    }

    public function firstRequest(Request $request)
    {
        GameManager::$user = Auth::user();
        if (GameManager::$user->game_id > 0) {
            $game = GameManager::getGame(GameManager::$user->game_id);
            if (GameManager::$game == null) return GameManager::postError('Такой игры нет.');
            GameManager::$gameUsers = GameManager::getCurrAndNextUsers(GameManager::$game);
            GameManager::$currentUser = GameManager::$gameUsers['currentUser'];
            GameManager::$nextUser = GameManager::$gameUsers['nextUser'];
            if (GameManager::$currentUser->id == GameManager::$user->id || GameManager::$nextUser->id == GameManager::$user->id) {
                GameManager::reloadLevel($game);
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ModalMessage($gameUser->id, 'Возвращение игрока', 'Участник игры случайно закрыл игру, но вернулся, поэтому игра была перезапущена.'));
                };
                return '';
            }
            return GameManager::postError('Ты не участник игры');
        }
        return ['users' => GameManager::getFreeUsersWithourUser()];
    }

    public function getUsers(Request $request)
    {
        return ['users' => GameManager::getFreeUsersWithourUser()];
    }

    public function invite(Request $request)
    {
        if (Auth::id() == $request->invitedId
            || InviteModel::where([
                ["inviting_id", '=', Auth::id()],
                ["invited_id", '=', $request->invitedId],
            ])->first()
        ) return GameManager::postError('Сам себя не пригласишь, либо приглашение уже существует');

        $user = User::find($request->invitedId);
        if ($user->game_id > 0) return GameManager::postError('Пользователь уже играет.');

        $invite = InviteModel::create([
            "inviting_id" => Auth::id(),
            "invited_id" => $request->invitedId
        ]);

        $invite = InviteModel::with('user')->find($invite->id);

        event(new IncomingInvite($invite, $request->invitedId));

        return '';
    }

    public function cancelOutgoingInvite(Request $request)
    {
        $invite = InviteModel::where([
            "inviting_id" => Auth::id(),
            "invited_id" => $request->invitedId
        ]);
        if ($invite) {
            $invite->delete();
        } else return GameManager::postError('Приглашения не существует');
        return '';
    }

    public function cancelIncomingInvite(Request $request)
    {
        $invite = InviteModel::find($request->id);
        if ($invite && $invite->invited_id == Auth::id()) {
            event(new CancelOutgoingInvite($invite->invited_id, $invite->inviting_id));
            $invite->delete();
        } else return GameManager::postError('Приглашения не существует, либо вы к нему не относитесь');
        return '';
    }

    public function confirmlInvite(Request $request)
    {
        $invite = InviteModel::find($request->id);

        $game = Game::where(
            [
                ['player1_id', '=', $invite->inviting_id],
                ['player2_id', '=', $invite->invited_id]
            ]
        )->first();

        if ($game) return GameManager::postError('Игра с этого приглашения уже существует.');

        GameManager::pushLevel($invite->inviting_id, $invite->invited_id);

        InviteModel::where('invited_id', $invite->invited_id)
            ->orWhere('invited_id', '>=', $invite->inviting_id)
            ->orWhere('inviting_id', '>=', $invite->invited_id)
            ->orWhere('inviting_id', '>=', $invite->inviting_id)
            ->delete();

        return '';
    }

    public function restartLevel(Request $request)
    {
        return GameManager::reloadLevel($request->gameId, true);
    }

    public function exitGame(Request $request)
    {
        $game = GameManager::$game;
        $player1Id = $game->player1_id;
        $player2Id = $game->player2_id;
        foreach (GameManager::$gameUsers as $gameUser) {
            $gameUser->game_id = 0;
            $gameUser->zone_id = 0;
        };
        $game->delete();

        $message1 = 'Вы вышли из игры. Можете найти кого-нибудь для новой игры.';
        $message2 = 'Вы вышли из игры. Можете найти кого-нибудь для новой игры.';
        if ($player1Id == GameManager::$userId) $message2 = 'Ваш напарник вышел игры. Можете найти кого-нибудь для новой игры.';
        if ($player2Id == GameManager::$userId) $message1 = 'Ваш напарник вышел игры. Можете найти кого-нибудь для новой игры.';

        $usersForPlayers = GameManager::getFreeUsersForPlayers($player1Id, $player2Id);

        event(new EndGame($player1Id, 'Конец игры', $message1, $usersForPlayers['usersForUser1']));
        event(new EndGame($player2Id, 'Конец игры', $message2, $usersForPlayers['usersForUser2']));

        return '';
    }

    public function move(Request $request)
    {
        $zone = Zone::with('weapons')
            ->with('monsters')
            ->find($request->id);

        if ($zone == null) return GameManager::postError('Зоны не существует');

        $user = GameManager::$currentUser;
        $user->zone_index = $zone->index;
        $user->save();

        $userId = $user->id;

        $monstersCount = $zone->monsters->count();

        $updateZone = false;

        $game = GameManager::$game;

        foreach (GameManager::$gameUsers as $gameUser) {
            event(new ChatMessage($gameUser->id, 'Игрок ' . $user->name . ' переместился в зону ' . $zone->index));
        };

        if ($zone->open == false) {
            $zone->open = true;
            foreach (GameManager::$gameUsers as $gameUser) {
                event(new ChatMessage($gameUser->id, 'Игрок ' . $user->name . ' открыл зону ' . $zone->index));
            };
            $updateZone = true;
        }

        if (GameManager::clearZone($monstersCount, $zone)) $updateZone = true;

        if ($updateZone) $zone->save();

        foreach (GameManager::$gameUsers as $gameUser) {
            event(new GoToZone($gameUser->id, $zone->index, $userId));
        };

        return '';
    }

    public function weaponEquip(Request $request)
    {
        $weapon = Weapon::find($request->id);
        if ($weapon) {
            if ($weapon->user_id != GameManager::$currentUser->id) return GameManager::postError('Оружие не в твоём инвентаре.');
            if (GameManager::$currentUser->weapon_id == $weapon->id) return GameManager::postError('Оружие уже экипировано.');
            GameManager::$currentUser->weapon_id = $weapon->id;
            GameManager::$currentUser->save();
            return '';
        }
        return GameManager::postError('В локации нет монстров.');
    }

    public function fight(Request $request)
    {
        $zone = Zone::with('monsters')->find($request->id);
        if ($zone->monsters->count() <= 0) return GameManager::postError('В локации нет монстров.');
        $user = GameManager::$currentUser;
        if ($zone->index != $user->zone_index) return GameManager::postError('Нельзя драться там, где тебя нет.');
        $user->battle = true;
        $user->save();
        foreach (GameManager::$gameUsers as $gameUser) {
            event(new ChatMessage($gameUser->id, 'Игрок ' . $user->name . ' вступил в бой в зоне ' . $user->zone_index));
            event(new Battle($gameUser->id, $user->id));
        };
        return '';
    }

    public function attack(Request $request)
    {
        $monster = Monster::with('zone')
            ->find($request->id);

        if ($monster == null) return GameManager::postError('Монстра нет');

        $currentUser = GameManager::$currentUser;

        if ($currentUser->zone_index != $monster->zone->index) return GameManager::postError('Ты не в локации с монстром');

        $nextUser = GameManager::$nextUser;

        $battleMembers = [$currentUser];

        if ($currentUser->id != $nextUser->id
            && $nextUser->battle
            && $nextUser->zone_index == $monster->zone->index
            && $nextUser->hp > 0
        )
            $battleMembers [] = $nextUser;

        $damage = 1;

        if ($currentUser->weapon) {
            $weapon = GameManager::getWeapon($currentUser->weapon->weapon_type_id);
            $damage += $weapon['damage'];
        }

        $rageAttackText = '';
        if ($request->rage) {
            $damage = random_int(0, $damage * 2);
            $rageAttackText = 'яростный';
        }

        $monsterWithParams = GameManager::getMonster($monster->monster_type_id);

        foreach (GameManager::$gameUsers as $gameUser) {
            event(new ChatMessage(
                $gameUser->id, 'Игрок ' . $currentUser->name . " нанёс $rageAttackText урон " . $damage . ' монстру ' . $monsterWithParams['name']
            ));
        };

        $monsterHp = $monster->hp - $damage;
        if ($monsterHp < 0) $monsterHp = 0;

        foreach (GameManager::$gameUsers as $gameUser) {
            event(new MonsterHp($gameUser->id, $monster->zone->index, $monster->id, $monsterHp));
        };

        if ($monsterHp > 0) {
            $monster->hp = $monsterHp;
            $monster->save();
        } else {
            foreach (GameManager::$gameUsers as $gameUser) {
                event(new ChatMessage(
                    $gameUser->id, 'Игрок ' . $currentUser->name . " убил монстра " . $monsterWithParams['name']
                ));
            };
            Monster::destroy($monster->id);
        }

        //////////
        $monstersZone = $monster->zone->monsters;

        foreach ($monstersZone as $monsterZone) {
            if ($monsterZone->hp > 0) {
                $targetUser = $currentUser;

                if (isset($battleMembers[1]) && random_int(1, 2) == 2) $targetUser = $nextUser;

                $monsterZoneWithParams = GameManager::getMonster($monsterZone->monster_type_id);
                $MonsterHp = $monsterZoneWithParams['damage'];

                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ChatMessage(
                        $gameUser->id, 'Монстр ' . $monsterZoneWithParams['name'] . " нанёс урон " . $MonsterHp . ' игроку ' . $targetUser->name
                    ));
                };

                $userHp = $targetUser->hp - $MonsterHp;

                if ($userHp <= 0) {
                    foreach (GameManager::$gameUsers as $gameUser) {
                        event(new ChatMessage(
                            $gameUser->id, 'Монстр ' . $monsterZoneWithParams['name'] . " убил игрока " . $targetUser->name
                        ));
                    };
                    event(new ModalMessage($targetUser->id, 'Вы мертвы!', 'Дождитесь, когда напарник пройдёт уровень/умрёт/перезапустит уровень.'));
                    $userHp = 0;
                }

                $targetUser->hp = $userHp;
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new UserHp($gameUser->id, $targetUser->id, $userHp));
                };
                $targetUser->save();
            }
        }

        $monstersCount = $monster->zone->monsters->count();
        if ($monstersCount == 0) {
            GameManager::clearZone($monstersCount, $monster->zone);
            foreach ($battleMembers as $battleMember) {
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ChatMessage(
                        $gameUser->id, 'Игрок ' . $battleMember->name . ' закончил свой бой в локации ' . $monster->zone->index
                    ));
                    if ($battleMember->hp > 0) {
                        $battleMember->hp = GameManager::$userHp;
                        $battleMember->battle = false;
                        $battleMember->save();
                        event(new UserHp($gameUser->id, $battleMember->id, GameManager::$userHp));
                    }
                    event(new Battle($gameUser->id, $battleMember->id, false));
                };
            };
        } else {
            if (count($battleMembers) == 1 && $battleMembers[0]->hp <= 0) {
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new Battle($gameUser->id, $battleMembers[0]->id, false));
                };
                foreach ($monstersZone as $monsterZone) {
                    $monsterZoneWithParams = GameManager::getMonster($monsterZone->monster_type_id);
                    if ($monsterZone->hp != $monsterZoneWithParams['hp']) {
                        $monsterZone->hp = $monsterZoneWithParams['hp'];
                        $monsterZone->save();
                    }
                }
            }

        }

        return '';
    }
}