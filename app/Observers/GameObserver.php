<?php

namespace App\Observers;

use App\Models\Game;
use App\Models\Monster;
use App\Models\User;
use App\Models\Weapon;
use App\Models\Zone;

class GameObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\Game $game
     * @return void
     */
    public function created(Game $game)
    {
        //
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function updated(Game $game)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function deleted(Game $game)
    {
        User::where('game_id', $game->id)->update(['game_id' => 0, 'zone_index' => 0]);
        Zone::where('game_id', $game->id)->delete();
        Weapon::where('game_id', $game->id)->delete();
        Monster::where('game_id', $game->id)->delete();
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function restored(Game $game)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function forceDeleted(Game $game)
    {
        //
    }
}
