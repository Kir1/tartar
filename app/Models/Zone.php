<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    public $timestamps = false;

    protected $fillable = ['game_id', 'index', 'open', 'notes'];

    public function weapons()
    {
        return $this->hasMany('App\Models\Weapon');
    }

    public function monsters()
    {
        return $this->hasMany('App\Models\Monster');
    }

    public function game()
    {
        return $this->belongsTo('App\Models\Game');
    }
}
