<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $fillable = ["inviting_id", "invited_id"];
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'inviting_id')->select('name', 'id');
    }
}