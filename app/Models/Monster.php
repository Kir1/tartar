<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Monster extends Model
{
    public $timestamps = false;

    protected $fillable = ['game_id', 'zone_id', 'hp', 'max_hp', 'monster_type_id'];

    public function zone()
    {
        return $this->belongsTo('App\Models\Zone');
    }
}