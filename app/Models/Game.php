<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public $timestamps = false;

    protected $fillable = ['player1_id', 'player2_id', 'level', 'move_player_id', 'open', 'count_moves', 'notes'];

    public function open_zones()
    {
        return $this->hasMany('App\Models\Zone')->where('open', true);
    }

    public function monsters()
    {
        return $this->hasMany('App\Models\Monster');
    }

    public function user1()
    {
        return $this->hasOne('App\Models\User', 'id', 'player1_id')
            ->select('id', 'name', 'hp', 'game_id', 'weapon_id', 'battle', 'zone_index');
    }

    public function user2()
    {
        return $this->hasOne('App\Models\User', 'id', 'player2_id')
            ->select('id', 'name', 'hp', 'game_id', 'weapon_id', 'battle', 'zone_index');
    }
}
