<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weapon extends Model
{
    public $timestamps = false;

    protected $fillable = ['game_id', 'zone_id', 'user_id', 'weapon_type_id'];
}