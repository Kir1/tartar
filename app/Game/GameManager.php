<?php

namespace App\Game;

use App\Events\ChatMessage;
use App\Events\ClearZone;
use App\Events\ModalMessage;
use App\Models\Monster;
use App\Models\User;
use App\Models\Weapon;
use App\Models\Zone;
use App\Models\Game;
use App\Events\ConstructLevel;
use App\Events\PlayerMove;
use Illuminate\Support\Facades\Auth;

class GameManager
{
    public static $userHp = 3;

    public static $userId = 0;

    public static $user = null;

    public static $game = null;

    public static $currentUser = null;

    public static $nextUser = null;

    public static $gameUsers = [];

    public static $countLevels = 2;

    /**
     * клонирование массива
     */
    public static function cloneArray(&$array)
    {
        $arrayObject = new \ArrayObject($array);
        $copy = $arrayObject->getArrayCopy();
        return $copy;
    }

    //МОНСТРЫ

    /**
     * бесы
     */
    public static $monsterFiends = [
        'type_id' => 1,
        'hp' => 3,
        'max_hp' => 3,
        'damage' => 1,
        'name' => 'Бесы',
        'image' => 'fiends'
    ];

    /**
     * чёрт
     */
    public static $monsterDickens = [
        'type_id' => 2,
        'hp' => 4,
        'max_hp' => 4,
        'damage' => 1,
        'name' => 'Чёрт',
        'image' => 'dickens'
    ];

    //ОРУЖИЕ
    /**
     * бита
     */
    public static $weaponBat = [
        'type_id' => 1,
        'damage' => 1,
        'name' => 'Бита',
        'image' => 'bite'
    ];

    /**
     * ломик
     */
    public static $weaponCrowbar = [
        'type_id' => 2,
        'damage' => 2,
        'name' => 'Ломик',
        'image' => 'сrowbar'
    ];

    public static function getWeapon($type_id)
    {
        switch ($type_id) {
            case 1:
                return GameManager::$weaponBat;
            case 2:
                return GameManager::$weaponCrowbar;
        }
    }

    public static function getMonster($type_id)
    {
        switch ($type_id) {
            case 1:
                return GameManager::$monsterFiends;
            case 2:
                return GameManager::$monsterDickens;
        }
    }

    public static function postResponse($message = 'Запрос прошёл нормально', $codeHttp = 200)
    {
        return \Response::json(array(
            'message' => $message
        ), $codeHttp);
    }

    public static function postError($message = 'Всё плохо', $codeError = 500)
    {
        return \Response::json(array(
            'message' => $message
        ), $codeError);
    }

    public static function absoluteImageUrl($imgUrl)
    {
        return $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/$imgUrl.jpg";
    }

    public static function getCurrAndNextUsers($game)
    {
        $users = ['currentUser' => null, 'nextUser' => null];
        if ($game->user1->id == $game->move_player_id) {
            $users['currentUser'] = $game->user1;
            $users['nextUser'] = $game->user2;
        } else {
            $users['currentUser'] = $game->user2;
            $users['nextUser'] = $game->user1;
        }
        return $users;
    }

    public static function getFreeUsersSql()
    {
        return User::where('game_id', 0)
            ->select('id', 'name')
            ->orderBy('name', 'desc')
            ->with('invite');
    }

    public static function getFreeUsersWithourUser()
    {
        $usersSql = GameManager::getFreeUsersSql();
        $users = $usersSql->where('id', '!=', Auth::id())->get();
        return $users;
    }

    public static function getFreeUsersForPlayers($user1Id, $user2Id)
    {
        $users = GameManager::getFreeUsersSql()->get();

        $usersForUser1 = $users->filter(function ($user, $key) use ($user1Id) {
            return $user->id != $user1Id;
        });

        $usersForUser2 = $users->filter(function ($user, $key) use ($user2Id) {
            return $user->id != $user2Id;
        });

        return ['usersForUser1' => $usersForUser1->values(), 'usersForUser2' => $usersForUser2->values()];
    }

    //УРОВНИ

    /**
     * создание зоны(переменная принимает массив, но все его элементы необязательны)
     * @param array $monsters монстры, клонируйте через функцию и переменные, пример GameManager::cloneArray(GameManager::$monsterFiends)
     * @param int $notes количество записок
     * @param array $weapons оружие, клонируйте через функцию и переменные, пример GameManager::cloneArray(GameManager::$weaponBat)
     * @param int $player какой игрок начнёт игру в этой зоне(0 - никто, 1 - создатель игры, 2 - приглашённый игрок)
     * @return array
     */
    public static function createZone($array = [])
    {
        $result = [
            'monsters' => [],
            'notes' => 0,
            'weapons' => []
        ];

        if (isset($array['monsters'])) $result['monsters'] = $array['monsters'];
        if (isset($array['notes'])) $result['notes'] = $array['notes'];
        if (isset($array['weapons'])) $result['weapons'] = $array['weapons'];

        return $result;
    }

    public static function clearZone($monstersCount, $zone)
    {
        $user = GameManager::$currentUser;
        $userId = $user->id;
        $updateZone = false;
        $game = GameManager::$game;
        if ($monstersCount == 0) {
            if ($zone->weapons->count() > 0) {
                Weapon::where('zone_id', $zone->id)->update(['zone_id' => 0, 'user_id' => $userId]);
                $updateZone = true;
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ChatMessage($gameUser->id, 'Игрок ' . $user->name . ' собрал оружие в зоне ' . $zone->index));
                };
            }
            if ($zone->notes > 0) {
                $zoneNotes = $zone->notes;
                $game->notes = $game->notes - $zoneNotes;
                $game->save();
                $zone->notes = 0;
                $updateZone = true;
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ChatMessage($gameUser->id, 'Игрок ' . $user->name . " собрал $zoneNotes записок в зоне " . $zone->index));
                };
            }
            if ($updateZone) {
                foreach (GameManager::$gameUsers as $gameUser) {
                    event(new ClearZone($gameUser->id, $zone->index, $userId));
                };
            }
        }
        return $updateZone;
    }

    public static function pushLevel($user1Id, $user2Id, $levelNum = 1, $openZones = [])
    {
        $createLevel = 'createLevel_' . $levelNum;

        $level = GameManager::$createLevel();
        $level['messages'] = [];
        $level['countNotes'] = 0;
        $level['countMonsters'] = 0;

        foreach ($level['zones'] as $index => &$zone) {
            $level['countNotes'] += $zone['notes'];
            $level['countMonsters'] += count($zone['monsters']);
        }

        $game = Game::create([
            "player1_id" => $user1Id,
            "player2_id" => $user2Id,
            "level" => $levelNum,
            "move_player_id" => $user1Id,
            "notes" => $level['countNotes']
        ]);

        $level['id'] = $game->id;

        foreach ($level['zones'] as $index => &$zone) {
            $openZone = false;
            if (isset($openZones[$index]))
                $openZone = true;
            else
                if ($level['user1ZoneIndex'] == $index || $level['user2ZoneIndex'] == $index) $openZone = true;

            $zoneModel = Zone::create([
                "game_id" => $game->id,
                "index" => $index,
                "open" => $openZone,
                "notes" => $zone['notes']
            ]);
            $zone['id'] = $zoneModel->id;
            $zone['index'] = $index;
            $zone['open'] = $openZone;

            if (empty($zone['monsters']) == false) foreach ($zone['monsters'] as &$monster) {
                $monsterModel = Monster::create([
                    "game_id" => $game->id,
                    "zone_id" => $zoneModel->id,
                    "hp" => $monster['hp'],
                    "max_hp" => $monster['max_hp'],
                    "monster_type_id" => $monster['type_id'],
                ]);
                $monster['image'] = GameManager::absoluteImageUrl('images/monsters/' . $monster['image']);
                $monster['id'] = $monsterModel->id;
            }

            if (empty($zone['weapons']) == false) foreach ($zone['weapons'] as &$weapon) {
                $weaponModel = Weapon::create([
                    "game_id" => $game->id,
                    "zone_id" => $zoneModel->id,
                    "weapon_type_id" => $weapon['type_id'],
                ]);
                $weapon['image'] = GameManager::absoluteImageUrl('images/weapons/' . $weapon['image']);
                $weapon['id'] = $weaponModel->id;
            }
        }

        $level['user1'] = User::select('hp', 'max_hp', 'id', 'name', 'zone_index', 'battle')->find($user1Id);
        $level['user1']->zone_index = $level['user1ZoneIndex'];
        $level['user1']->game_id = $game->id;
        $level['user1']->hp = GameManager::$userHp;
        $level['user1']->battle = false;
        $level['user1']->save();
        $level['user1']->weapons = [];

        $level['user2'] = User::select('hp', 'max_hp', 'id', 'name', 'zone_index', 'battle')->find($user2Id);
        $level['user2']->zone_index = $level['user2ZoneIndex'];
        $level['user2']->game_id = $game->id;
        $level['user2']->hp = GameManager::$userHp;
        $level['user1']->battle = false;
        $level['user2']->save();
        $level['user2']->weapons = [];

        $level['messages'] = [];

        $level['movePlayerId'] = $user1Id;

        $game->count_moves = $level['countMoves'];
        $game->notes = $level['countNotes'];
        $game->save();

        event(new ConstructLevel($level, $user1Id));
        event(new ConstructLevel($level, $user2Id));
        event(new PlayerMove($user1Id));
    }

    public static function getGame($gameId)
    {
        GameManager::$game = Game::with('monsters')->with('user1')->with('user2')->find($gameId);
        return GameManager::$game;
    }

    public static function reloadLevel($gameId, $reloadUser = false)
    {
        $game = GameManager::$game;

        $level = $game->level;

        $openZones = $game->open_zones;

        if ($openZones->count() > 0) $openZones = $openZones->keyBy('index')->toArray();

        $id1 = $game->player1_id;
        $id2 = $game->player2_id;

        GameManager::pushLevel($id1, $id2, $level, $openZones);

        if ($reloadUser) {
            event(new ModalMessage($id1, 'Перезагрузка уровня', 'Игра была перезапущена участником игры.'));
            event(new ModalMessage($id2, 'Перезагрузка уровня', 'Игра была перезапущена участником игры.'));
        }

        $game->delete();

        return '';
    }

    //ВСЕ УРОВНИ КАРТОЙ 6 НА 6

    public static function createLevel_2()
    {
        $level = [];
        $level['zones'] = [
            0 => GameManager::createZone(),
            1 => GameManager::createZone(['notes' => 1]),
            2 => GameManager::createZone(),
            3 => GameManager::createZone(['notes' => 1]),
            4 => GameManager::createZone(),
        ];

        $level['map'] = [
            ['-', 4, 3, '-'],
            [0, 1, 2, '-'],
        ];

        $level['user1ZoneIndex'] = 0;
        $level['user2ZoneIndex'] = 4;

        $level['countMoves'] = 2;

        return $level;
    }

    public static function createLevel_1()
    {
        $level = [];
        $level['zones'] = [
            0 => GameManager::createZone(['notes' => 1]),
            1 => GameManager::createZone(['notes' => 1]),
            2 => GameManager::createZone(['notes' => 1]),
            3 => GameManager::createZone([
                'monsters' => [
                    GameManager::cloneArray(GameManager::$monsterFiends),
                    GameManager::cloneArray(GameManager::$monsterFiends),
                    GameManager::cloneArray(GameManager::$monsterFiends)
                ]
            ]),
            4 => GameManager::createZone([
                'weapons' => [
                    GameManager::cloneArray(GameManager::$weaponBat)
                ]
            ]),
            5 => GameManager::createZone([
                'monsters' => [
                    GameManager::cloneArray(GameManager::$monsterDickens)
                ],
                'weapons' => [
                    GameManager::cloneArray(GameManager::$weaponBat)
                ]
            ]),
            6 => GameManager::createZone(['notes' => 1]),
            7 => GameManager::createZone(),
            8 => GameManager::createZone(),
            9 => GameManager::createZone(),
            10 => GameManager::createZone(),
            11 => GameManager::createZone(['notes' => 1])
        ];

        $level['user1ZoneIndex'] = 8;
        $level['user2ZoneIndex'] = 9;

        $level['map'] = [
            [0, '-', 11, '-', '-'],
            [1, '-', '-', '-', '-'],
            [2, 5, 6, 7, 8],
            [3, '-', '-', '-', 9],
            [4, '-', '-', 10, '-'],
        ];

        $level['countMoves'] = 19;

        return $level;
    }
}