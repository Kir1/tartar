<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' =>'Gamer1',
            'email' => 'gamer1@mail.com',
            'password' => Hash::make('TraleRSox111')
        ]);
        User::create([
            'name' =>'Gamer2',
            'email' => 'gamer2@mail.com',
            'password' => Hash::make('TraleRSox111')
        ]);
    }
}
