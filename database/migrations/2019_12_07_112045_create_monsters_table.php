<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonstersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monsters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('game_id')->default(0);
            $table->bigInteger('zone_id')->default(0);
            $table->tinyInteger('hp')->default(3);
            $table->tinyInteger('max_hp')->default(3);
            $table->bigInteger('monster_type_id')->default(0);

            $table->index('game_id');
            $table->index('zone_id');
            $table->index('hp');
            $table->index('max_hp');
            $table->index('monster_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monsters');
    }
}
