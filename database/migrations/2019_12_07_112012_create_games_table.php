<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('player1_id')->default(0);
            $table->bigInteger('player2_id')->default(0);
            $table->bigInteger('move_player_id')->default(0);
            $table->smallInteger('level')->default(1);
            $table->smallInteger('count_moves')->default(1);
            $table->smallInteger('notes')->default(1);
            $table->boolean('open')->default(1);

            $table->index('player1_id');
            $table->index('player2_id');
            $table->index('move_player_id');
            $table->index('level');
            $table->index('count_moves');
            $table->index('notes');
            $table->index('open');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
