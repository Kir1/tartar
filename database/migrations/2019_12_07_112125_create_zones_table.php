<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('game_id')->default(0);
            $table->boolean('open')->default(0);
            $table->bigInteger('index')->default(0);
            $table->tinyInteger('notes')->default(0);

            $table->index('game_id');
            $table->index('open');
            $table->index('index');
            $table->index('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zones');
    }
}
