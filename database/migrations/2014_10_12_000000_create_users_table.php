<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->tinyInteger('hp')->default(3);
            $table->tinyInteger('max_hp')->default(3);
            $table->bigInteger('game_id')->default(0);
            $table->bigInteger('weapon_id')->default(0);
            $table->bigInteger('zone_index')->default(0);
            $table->boolean('battle')->default(0);

            $table->index('name');
            $table->index('game_id');
            $table->index('weapon_id');
            $table->index('zone_index');
            $table->index('battle');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
