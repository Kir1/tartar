<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeaponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weapons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('game_id')->default(0);
            $table->bigInteger('zone_id')->default(0);
            $table->bigInteger('user_id')->default(0);
            $table->bigInteger('weapon_type_id')->default(0);

            $table->index('game_id');
            $table->index('zone_id');
            $table->index('user_id');
            $table->index('weapon_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weapons');
    }
}
