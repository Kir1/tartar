export function bindEvents($this, namesFunctions) {
    namesFunctions.forEach(nameFunction => $this[nameFunction] = $this[nameFunction].bind($this));
}

export function getImageUrl(url) {
    let arr = window.location.href.split("/");
    return arr[0] + "//" + arr[2] + '/' + url + '.jpg'
}

export function getMonsterIcon(url) {
    return url.replace('.jpg','Icon.jpg')
}

export function checkUserCreator(checkUserCreatorFunction) {
    let checkUserCreator = {result: false};
    checkUserCreatorFunction(checkUserCreator);
    return checkUserCreator.result;
}