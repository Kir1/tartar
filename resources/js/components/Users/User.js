import {bindEvents} from '../../helpers';
import {pushInvite, cancelOutgoingInvite} from '../gameManager';
import {connect} from 'react-redux';
import React from 'react';

class User extends React.Component {
    constructor(props) {
        super(props);
        bindEvents(this, ['pushInvite', 'cancelOutgoingInvite']);
    }

    pushInvite() {
        this.props.pushInvite(this.props.user.id);
    }

    cancelOutgoingInvite() {
        this.props.cancelOutgoingInvite(this.props.user.id);
    }

    render() {
        let text = 'Отменить приглашение',
            classButton = 'cancel',
            clickFunction = this.cancelOutgoingInvite;

        if (this.props.user.invite == undefined) {
            text = 'Отправить приглашение';
            classButton = '';
            clickFunction = this.pushInvite;
        }

        return (
            <div className="user">
                <h2>{this.props.user.name}</h2>
                <div className={"user-button " + classButton} onClick={clickFunction}>
                    {text}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    users: state.game.users
})

const mapDispatchToProps = dispatch => ({
    pushInvite: (invitedId) => dispatch(pushInvite(invitedId)),
    cancelOutgoingInvite: (userId) => dispatch(cancelOutgoingInvite(userId)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User)