import User from './User';
import IncomingInvite from './IncomingInvite';
import ScrollArea from 'react-scrollbar';
import {getUsers} from "../gameManager";
import {Redirect} from "react-router-dom";
import {connect} from 'react-redux';
import React from 'react';

class UsersPage extends React.Component {
    constructor() {
        super();
    }

    render() {
        if (this.props.level) return <Redirect to="/game"/>

        return (
            <div className="user-page page">
                <h1>Игроки:</h1>

                <div className="users">
                    <div className="reload-users" onClick={this.props.getUsers}>Обновить</div>
                    <ScrollArea
                        speed={0.8}
                        contentClassName="content"
                        horizontal={false}
                        style={{height: '800px'}}
                    >
                        {this.props.users && this.props.users.length > 0 ?
                            this.props.users.map(user => (
                                <User key={user.id} user={user}/>
                            ))
                            :
                            <h3>В игре пока нет свободных игроков.</h3>
                        }
                    </ScrollArea>
                </div>

                {this.props.invites && this.props.invites.length > 0 &&
                this.props.invites.map(invite => (
                    <IncomingInvite key={invite.user.id} invite={invite}/>
                ))
                }
                <a className="exit-button" href='/logout'>Выйти</a>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    users: state.game.users,
    invites: state.game.invites,
    level: state.game.level
})

const mapDispatchToProps = dispatch => ({
    getUsers: () => dispatch(getUsers()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UsersPage)