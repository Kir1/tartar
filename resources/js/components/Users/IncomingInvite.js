import {bindEvents} from '../../helpers';
import {cancelIncomingInvite, confirmInvite} from '../gameManager';
import {connect} from 'react-redux';
import React from 'react';

class IncomingInvite extends React.Component {
    constructor(props) {
        super(props);
        bindEvents(this, ['confirmInvite', 'cancelIncomingInvite']);
    }

    cancelIncomingInvite() {
        this.props.cancelIncomingInvite(this.props.invite.id);
    }

    confirmInvite() {
        this.props.confirmInvite(this.props.invite.id);
    }

    render() {
        return (
            <div className="modal-invite">
                <div className="darker"></div>
                <div className="modal-invite-container">
                    <h2 className="modal-invite-title">
                        Вам пришло приглашение от пользователя {this.props.invite.user.name}
                    </h2>
                    <div className="user-button" onClick={this.confirmInvite}>
                        Принять
                    </div>
                    <div className="user-button cancel" onClick={this.cancelIncomingInvite}>
                        Отменить
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
    cancelIncomingInvite: (id) => dispatch(cancelIncomingInvite(id)),
    confirmInvite: (id) => dispatch(confirmInvite(id))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IncomingInvite)