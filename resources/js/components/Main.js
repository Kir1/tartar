import GamePage from './Game/GamePage'
import UsersPage from './Users/UsersPage';
import {
    firstRequest,
    constructLevel,
    echoGoToZone,
    echoPlayerMove,
    echoClearZone,
    echoEndGame,
    changeModalText,
    addChatMessage,
    echoBattle,
    echoMonsterHp,
    echoUserHp,
    INIT_USER_APP,
    ECHO_INVITE_HAS_COME,
    CANCEL_OUTGOING_INVITE,
    ECHO_REDUCTION_MOVES
} from './gameManager';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Modal from 'react-modal';
import {connect} from 'react-redux';
import React from 'react';

class Main extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        this.props.initUserApp(this.props.userId);
        Echo.join('user.' + this.props.userId)
            .here((users) => {
                this.props.firstRequest();
            })
            .listen('IncomingInvite', (e) => {
                this.props.echoInvite(e.invite);
            })
            .listen('CancelOutgoingInvite', (e) => {
                this.props.echoCancelOutgoingInvite(e.invitedId);
            })
            .listen('ConstructLevel', (e) => {
                this.props.echoConstructLevel(e.level);
            })
            .listen('EndGame', (e) => {
                this.props.echoEndGame(e.title, e.text, e.users);
            })
            .listen('ModalMessage', (e) => {
                this.props.echoModalMessage(e.title, e.text);
            })
            .listen('GoToZone', (e) => {
                this.props.echoGoToZone(e.userId, e.zoneIndex);
            })
            .listen('ClearZone', (e) => {
                this.props.echoClearZone(e.winnerId, e.zoneIndex);
            })
            .listen('PlayerMove', (e) => {
                this.props.echoPlayerMove(e.userChannelId);
            })
            .listen('ReductionMoves', (e) => {
                this.props.echoReductionMoves();
            })
            .listen('ChatMessage', (e) => {
                this.props.addChatMessage(e.text);
            })
            .listen('Battle', (e) => {
                this.props.echoBattle(e.userId, e.battle);
            })
            .listen('MonsterHp', (e) => {
                this.props.echoMonsterHp(e.zoneIndex, e.monsterId, e.hp);
            })
            .listen('UserHp', (e) => {
                this.props.echoUserHp(e.userId, e.hp);
            })
    }

    render() {
        let darkFull = '';
        if (this.props.requestDarkFull) darkFull = 'full';

        return (
            <>
            {this.props.requestPull &&
            (<div className={"dark " + darkFull}>
                <div className="textDark">Ожидание...</div>
                <div className={"background " + darkFull}></div>
            </div>)
            }
            <div className="game-background"></div>
            <Router>
                <Switch>
                    <Route exact path="/" component={UsersPage}/>
                    <Route path="/game" component={GamePage}/>
                </Switch>
            </Router>

            <Modal
                isOpen={this.props.textModal.title.length > 0}
                contentLabel={this.props.textModal.title}
                ariaHideApp={false}
                className="main-modal"
            >
                <div className="game-modal">
                    <div className="close" onClick={this.props.closeModal}>X</div>
                    <h3>{this.props.textModal.title}</h3>
                    <div dangerouslySetInnerHTML={{__html: this.props.textModal.text}}/>
                </div>
            </Modal>
            </>
        );
    }
}

const mapStateToProps = state => ({
    requestPull: state.game.requestPull,
    requestDarkFull: state.game.requestDarkFull,
    textModal: state.game.textModal
})

const mapDispatchToProps = dispatch => ({
    firstRequest: () => dispatch(firstRequest()),
    initUserApp: (userId) => dispatch({type: INIT_USER_APP, userId}),
    echoInvite: (invite) => dispatch({type: ECHO_INVITE_HAS_COME, invite}),
    echoCancelOutgoingInvite: (invitedId) => dispatch({type: CANCEL_OUTGOING_INVITE, invitedId}),
    echoConstructLevel: (level) => dispatch(constructLevel(level)),
    echoPlayerMove: (userId) => dispatch(echoPlayerMove(userId)),
    echoGoToZone: (userId, zoneIndex) => dispatch(echoGoToZone(userId, zoneIndex)),
    echoClearZone: (userId, zoneIndex) => dispatch(echoClearZone(userId, zoneIndex)),
    echoEndGame: (title, text, users) => dispatch(echoEndGame(title, text, users)),
    echoModalMessage: (title, text) => dispatch(changeModalText(title, text, true)),
    addChatMessage: (text) => dispatch(addChatMessage(text)),
    echoBattle: (userId, battle) => dispatch(echoBattle(userId, battle)),
    echoMonsterHp: (zoneIndex, monsterId, hp) => dispatch(echoMonsterHp(zoneIndex, monsterId, hp)),
    echoUserHp: (userId, hp) => dispatch(echoUserHp(userId, hp)),
    echoReductionMoves: () => dispatch({type: ECHO_REDUCTION_MOVES}),
    closeModal: () => dispatch(changeModalText()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)