import './SPA.scss';
import {game} from './gameManager';
import Main from './Main';
import {Provider} from 'react-redux';
import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import React from 'react';
import ReactDOM from 'react-dom';

const rootReducer = combineReducers({
    game
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

class SPA extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Main userId = {this.props.userId} />
            </Provider>
        );
    }
}

let spaContainer = document.getElementById('SPA');

if (spaContainer) {
    ReactDOM.render(<SPA userId={spaContainer.getAttribute("userId")}/>, spaContainer);
}
