import Item from './Game/Item';
import ScrollArea from 'react-scrollbar';
import Modal from 'react-modal';
import {checkUserCreator as checkUserCreatorState} from '../components/gameManager';
import {bindEvents, checkUserCreator} from '../helpers';
import React from 'react';
import {connect} from 'react-redux';

class Wrapper extends React.Component {
    constructor() {
        super();
        this.state = {
            showModal: false
        };
        bindEvents(this, ['handleOpenModal', 'handleCloseModal']);
    }

    handleOpenModal() {
        this.setState({showModal: true});
    }

    handleCloseModal() {
        this.setState({showModal: false});
    }

    render() {
        let userCreator = checkUserCreator(this.props.checkUserCreatorState);

        let player1Obj, player2Obj, user1Class = '', user2Class = '';

        if (userCreator) {
            player1Obj = this.props.player1;
            player2Obj = this.props.player2;
            user1Class = 'name-player1';
            user2Class = 'name-player2';
        } else {
            player1Obj = this.props.player2;
            player2Obj = this.props.player1;
            user1Class = 'name-player2';
            user2Class = 'name-player1';
        }

        let style2 = {}, endInfo2 = '', player2;
        if (player2Obj.hp == 0) {
            style2 = {color: 'red'};
            endInfo2 = 'мёртв';
        } else {
            endInfo2 = '-  (Жизни: ' + player2Obj.hp + '/' + player2Obj.max_hp + ')';
        }
        player2 = <span style={style2}>Напарник {player2Obj.name} {endInfo2}</span>;

        let chatMessages = '';
        if (this.props.messages.length > 0) {
            let messsages = this.props.messages.map((message, i) => (
                <div key={i}>{message}</div>
            ));

            messsages = messsages.reverse();

            chatMessages =
                <ScrollArea
                    speed={0.8}
                    contentClassName="chat-container"
                    horizontal={false}
                >
                    {messsages}
                </ScrollArea>;
        }

        return (
            <div className="wrapper">
                <div className="character-list">
                    <div className="block1">
                        <div className="faq" onClick={this.handleOpenModal}>FAQ</div>
                        <Modal
                            isOpen={this.state.showModal}
                            contentLabel="Руководство"
                            ariaHideApp={false}
                            className="main-modal"
                        >
                            <div className="faq-modal">
                                <div className="close" onClick={this.handleCloseModal}>X</div>

                                <h2>Суть игры</h2>
                                <div>
                                    Два игрока в пошаговом режиме проходят игровые уровни, они перемещаются по
                                    локациям/зонам, собирают записки, оружие, сражаются с монстрами. Когда все локации
                                    зачищены, происходит переход на следующий уровень, но пройти его нужно за
                                    определённое количество ходов, что получится сделать только за несколько
                                    прохождений. Любой игрок может выйти из игры или
                                    перезапустить уровень в любой момент, при этом открыте зоны останутся открытыми.
                                    Также будет в случае смерти игроков. Не всегда смерть одного из игроков значит
                                    неудачное прохождение, иногда второй игрок сможет закончить её в одиночку.
                                </div>

                                <h2>Перемещение и зоны</h2>
                                <div>
                                    Изначально игроки не видят, что находится в игровых зонах, но если хотя бы один из
                                    них переместиться в ту или иную зону, то оба будут видеть, что в ней находится.
                                    Игрок может переместиться из любой зоны в любую зону. Если в зоне монстры, то игрок
                                    может вступить с ними в бой. Если монстров нет, но есть вещи, то они будут
                                    автоматически подобраны.
                                </div>

                                <h2>Персонаж и инвентарь</h2>
                                <div>
                                    У персонажа игрока есть количество жизней и инвентарь оружия.
                                    Смена оружия не считается за действие, но в бою это сделать нельзя.
                                </div>

                                <h2>Бой</h2>
                                <div>
                                    Игрок сам решает, когда вступить в бой с монстрами в зоне, в которой он находится.
                                    Начало боя не считается за действие.
                                    За действия считаются атаки игрока.
                                    Из боя нельзя выйти - либо победа, либо смерть.
                                    Если игрок вступает в бой другого игрока, , вступивший в бой другого игрока,
                                    то он ходит перед монстрами.
                                    После успешного боя жизни игрока/игроков восстанавливаются сами, а
                                    награду получит игрок, добивший последнего монстра.
                                    Если в бою победили монстры, то выжившие из них восстановят свои жизни.
                                    Вместо обычной атаки можно использовать яростную атаку.
                                    Яростная атака - это альтернативная атака, она наносит случайный урон от 0 до x,
                                    где x - это урон от оружия игрока, умноженный на два.
                                    Использование яростной атаки необязательно для прохождения, но если вы азартны и
                                    удачливы,
                                    то игра станет проще и веселее для вас.
                                </div>
                            </div>
                        </Modal>
                        <div className={user2Class}>{player2}</div>
                    </div>

                    <div className="block2">
                        <h2 className={user1Class}>
                            {player1Obj.name} -
                            {
                                player1Obj.hp > 0 ? (
                                    <>(Жизни: {player1Obj.hp}/{player1Obj.max_hp})</>
                                ) : (
                                    <>мёртв</>
                                )
                            }
                        </h2>

                        <h3>Инвентарь:</h3>
                        <div className="inventory-container">
                            <ScrollArea
                                speed={0.8}
                                contentClassName="content"
                                horizontal={false}
                                style={{height: '625px'}}
                            >
                                <div className="inventory">
                                    {player1Obj.weapons && player1Obj.weapons.length > 0 &&
                                    player1Obj.weapons.map(item => (
                                        <Item key={item.id} item={item}/>
                                    ))
                                    }
                                </div>
                            </ScrollArea>
                        </div>

                    </div>
                </div>
                <div className="main-block">
                    <div className="content">
                        {this.props.children}
                    </div>
                    <div className="chat">
                        {chatMessages}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    player1: state.game.level.user1,
    player2: state.game.level.user2,
    messages: state.game.level.messages
})

const mapDispatchToProps = dispatch => ({
    checkUserCreatorState: (obj) => dispatch(checkUserCreatorState(obj)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Wrapper)