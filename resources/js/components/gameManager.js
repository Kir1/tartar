import axios from "axios";
import {getImageUrl} from "../helpers";

////ACTIONS
export const INIT_USER_APP = 'INIT_USER_APP';

//all
export const CHANGE_MODAL_TEXT = 'CHANGE_MODAL_TEXT';
export const REQUEST_ERROR = 'REQUEST_ERROR';
export const FULL_DARK = 'FULL_DARK';
export const OPACITY_DARK = 'OPACITY_DARK';
export const SHOW_DARK = 'SHOW_DARK';
export const HIDE_DARK = 'HIDE_DARK';
///users list
export const GET_USERS = 'GET_USERS';

export const PUSH_INVITE = 'PUSH_INVITE';

export const CANCEL_OUTGOING_INVITE = 'CANCEL_OUTGOING_INVITE';
export const CANCEL_INCOMING_INVITE = 'CANCEL_INCOMING_INVITE';

export const CLEAR_INVITES = 'CLEAR_INVITES';

export const ECHO_INVITE_HAS_COME = 'ECHO_INVITE_HAS_COME';

///game
export const ECHO_ADD_CHAT_MESSAGE = "ECHO_ADD_CHAT_MESSAGE";

export const ECHO_CONSTRUCT_LEVEL = 'ECHO_CONSTRUCT_LEVEL';
export const ECHO_END_GAME = 'ECHO_END_GAME';
export const ECHO_PLAYER_MOVE = 'ECHO_PLAYER_MOVE';
export const ECHO_REDUCTION_MOVES = 'ECHO_REDUCTION_MOVES';
export const ECHO_GO_TO_ZONE = 'ECHO_GO_TO_ZONE';
export const ECHO_CLEAR_ZONE = 'ECHO_CLEAR_ZONE';

export const ECHO_MONSTER_DAMAGE = 'ECHO_MONSTER_DAMAGE';
export const ECHO_USER_DAMAGE = 'ECHO_USER_DAMAGE';

export const EQUIP_WEAPON = 'EQUIP_WEAPON';
export const UNEQUIP_WEAPON = 'UNEQUIP_WEAPON';

export const ECHO_BATTLE = 'ECHO_BATTLE';

////////FUNCTIONS
//users list
export function getUsersFun(dispatch, url = 'get-users') {
    return new Promise((resolve, reject) => {
        axios
            .post("/" + url)
            .then(data => {
                if (data.data.users)
                    dispatch({
                        type: GET_USERS,
                        users: data.data.users
                    });
                resolve('get user succes');
            })
            .catch(e => dispatch({type: REQUEST_ERROR}))
    });
}
export function firstRequest() {
    return function (dispatch, getState) {
        let requestFun = () => getUsersFun(dispatch, 'first');
        let finalFun = () => {
            dispatch({type: OPACITY_DARK});
        };
        dispatch(requestWithDark(requestFun, finalFun));
    };
}
export function getUsers() {
    return function (dispatch, getState) {
        let requestFun = () => getUsersFun(dispatch);
        dispatch(requestWithDark(requestFun));
    };
}
export function pushInvite(invitedId) {
    return function (dispatch, getState) {
        dispatch({type: PUSH_INVITE, invitedId});
        axios.post("/invite", {invitedId}).catch(e => dispatch({type: REQUEST_ERROR}));
    };
}
export function cancelOutgoingInvite(invitedId) {
    return function (dispatch, getState) {
        dispatch({type: CANCEL_OUTGOING_INVITE, invitedId});
        axios.post("/cancel-outgoing-invite", {invitedId}).catch(e => dispatch({type: REQUEST_ERROR}));
    };
}
export function cancelIncomingInvite(id) {
    return function (dispatch, getState) {
        dispatch({type: CANCEL_INCOMING_INVITE, id});
        axios.post("/cancel-incoming-invite", {id}).catch(e => dispatch({type: REQUEST_ERROR}));
    };
}
export function confirmInvite(id) {
    return function (dispatch, getState) {
        let requestFun = () => {
            axios.post("/confirm-invite", {id})
                .then(data => {
                    dispatch({type: CLEAR_INVITES});
                })
                .catch(e => dispatch({type: REQUEST_ERROR}))
        };
        dispatch(requestWithDark(requestFun));
    };
}

export function constructLevel(level) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_CONSTRUCT_LEVEL, level});
    };
}

export function exit() {
    return function (dispatch, getState) {
        dispatch(simlpeRequest('exit-game'));
    };
}

export function restart() {
    return function (dispatch, getState) {
        dispatch(simlpeRequest('restart-level'));
    };
}

export function goToZone(id) {
    return function (dispatch, getState) {
        dispatch(simlpeRequest('move', {id}));
    };
}

export function figthZone(id) {
    return function (dispatch, getState) {
        dispatch(simlpeRequest('fight', {id}, false));
    };
}

export function attackMonster(id, rage = false) {
    return function (dispatch, getState) {
        dispatch(simlpeRequest('attack', {id, rage}));
    };
}

export function equip(id) {
    return function (dispatch, getState) {
        let state = getState();
        let requestFun = () => {
            axios.post("/weapon-equip", {id, gameId: state.game.level.user1.game_id})
                .then(data => {
                    dispatch({type: EQUIP_WEAPON, id});
                })
                .catch(e => {
                    dispatch({type: REQUEST_ERROR});
                })
        };
        dispatch(requestWithDark(requestFun));
    };
}

export function changeModalText(title = '', text = '', lastMessage = false) {
    return function (dispatch, getState) {
        dispatch({type: CHANGE_MODAL_TEXT, title, text});
    };
}

export function addChatMessage(text) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_ADD_CHAT_MESSAGE, text});
    };
}

export function checkUserCreator(resultObject) {
    return function (dispatch, getState) {
        let state = getState();
        if (state.game.level) resultObject.result = state.game.level.creatorId == state.game.level.user1.id;
        else resultObject.result = false;
    };
}

export function getMonstersInFigth(resultObject) {
    return function (dispatch, getState) {
        let state = getState();
        let user = getUser(state.game.level, state.game.userId);
        resultObject.monsters = [];
        if (user.battle) {
            let zone = getZone(state.game.level, user.zone_index);
            if (zone.monsters.length > 0) resultObject.monsters = [...zone.monsters];
        }
    };
}

//
export function requestWithDark(requestFun, finalFun = null) {
    return function (dispatch, getState) {
        let promise = new Promise((resolve, reject) => {
            dispatch({type: SHOW_DARK});
            resolve(true);
        });
        promise
            .then(
                result => {
                    return requestFun();
                }
            )
            .then(
                result => {
                    dispatch({type: HIDE_DARK});
                    if (finalFun) finalFun();
                }
            );
    };
}

export function echoMonsterHp(zoneIndex, monsterId, hp) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_MONSTER_DAMAGE, zoneIndex, monsterId, hp});
    };
}

export function echoUserHp(userId, hp) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_USER_DAMAGE, userId, hp});
    };
}

export function echoGoToZone(userId, zoneIndex) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_GO_TO_ZONE, userId, zoneIndex});
    };
}

export function echoClearZone(userId, zoneIndex) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_CLEAR_ZONE, userId, zoneIndex});
    };
}

export function echoPlayerMove(userId) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_PLAYER_MOVE, userId});
    };
}

export function echoEndGame(title, text, users) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_END_GAME, title, text, users});
    };
}

export function echoBattle(userId, battle) {
    return function (dispatch, getState) {
        dispatch({type: ECHO_BATTLE, userId, battle});
    };
}

function simlpeRequest(url, objParams = {}, withDark = true) {
    return function (dispatch, getState) {
        if (withDark) dispatch({type: SHOW_DARK});
        let state = getState();
        objParams.gameId = state.game.level.user1.game_id;
        axios.post("/" + url, objParams).catch(e => {
            dispatch({type: HIDE_DARK});
            dispatch({type: REQUEST_ERROR});
        });
    };
}

//
function createWeapon(id = 1) {
    return {
        image: getImageUrl('images/weapons/bite'),
        damage: 3,
        id,
        name: 'Бита',
        type_id: 1
    }
}

function createMonster(id = 1) {
    return {
        image: getImageUrl('images/monsters/fiends'),
        damage: 1,
        hp: 3,
        max_hp: 3,
        id,
        name: 'Бес',
        type_id: 1
    }
}

function createZone(id = 0, open = false, notes = 0, weapons = [], monsters = []) {
    return {
        monsters,
        weapons,
        notes,
        id,
        open,
    }
}

function createUser(id = 1, name = 'Player1', zone_index = 0) {
    let items = [];

    for (var i = 1; i < 30; i++) {
        items.push(createWeapon(i));
    }

    return {
        hp: 3,
        max_hp: 3,
        id,
        name,
        zone_index,
        battle: false,
        weapon_id: 2,
        items
    }
}

function createLevel() {
    let zones = [
            createZone(0, true),
            createZone(1, true, 1),
            createZone(2, true, 1, [createWeapon(600), createWeapon(700)], [createMonster(), createMonster()]),
            createZone(3),
            createZone(4)
        ],
        map = [
            ['---', '---', '---', 0],
            ['---', 1, 2, '---'],
            ['---', '---', 3, '---'],
            ['---', '---', '---', 4]
        ];

    return {
        id: 0,
        user1: createUser(1, 'Player1', 2),
        user2: createUser(2, 'Player2', 2),
        countNotes: 0,
        countMoves: 0,
        countMonsters: 0,
        zones,
        movePlayerId: 0,
        map,
        messages: [],
        creatorId: 0
    }
}

function getUser(level, userId) {
    let user = level.user1;
    if (level.user2.id == userId) user = level.user2;
    return user;
}

function getUserApp(obj) {
    return function (dispatch, getState) {
        let state = getState();
        obj.user = getUser(state.game.level, state.game.userId);
    };
}

function setUserField(level, userId, fieldName, fieldValue) {
    let user = getUser(level, userId);
    user[fieldName] = fieldValue;
    return level;
}

function getZone(level, zoneIndex) {
    let result = false;
    level.zones.some(function (zone) {
        if (zone.index == zoneIndex) {
            result = zone;
            return true;
        }
    });
    return result;
}

function openZone(level, zoneIndex) {
    let zone = getZone(level, zoneIndex);
    zone.open = true;
    return level;
}

function clearZone(level, userId, zoneIndex) {
    let user = getUser(level, userId);
    let zone = getZone(level, zoneIndex);

    if (zone) {
        level.countNotes -= zone.notes;
        level.countMonsters -= zone.monsters.length;
        zone.weapons.forEach(weapon => level.messages.push('Игрок ' + user.name + ' получил предмет - ' + weapon.name));
        user.weapons = [...user.weapons].concat([...zone.weapons]);
        zone.weapons = [];
        zone.monsters = [];
        zone.notes = 0;
    }
    return level;
}

//////REDUCER
const initialState = {
    userId: 0,
    users: [],
    textModal: {title: '', text: ''},
    requestPull: true,
    requestDarkFull: true,
    invites: [],
    level: null //createLevel()
};
export const game = (state = initialState, action) => {
    switch (action.type) {
        case FULL_DARK:
            return {...state, requestDarkFull: true};
        case OPACITY_DARK:
            return {...state, requestDarkFull: false};
        case SHOW_DARK:
            return {...state, requestPull: true};
        case HIDE_DARK:
            return {...state, requestPull: false};

        case INIT_USER_APP:
            return {...state, userId: Number(action.userId)};

        case GET_USERS:
            var users = [];
            if (action.users != undefined) users = action.users;
            return {...state, users};

        case PUSH_INVITE:
            var users = [...state.users], userId, invitedId = Number(action.invitedId);

            users.some(function (user) {
                userId = Number(user.id);
                if (
                    userId == invitedId
                ) {
                    user.invite = {invitingId: Number(state.userId), invitedId};
                    return true;
                }
            });
            return {...state, users};
        case CANCEL_OUTGOING_INVITE:
            var users = [...state.users],
                invitingId = Number(state.userId),
                invitedId = Number(action.invitedId),
                userInvitingId,
                userinvitedId
            ;

            users.some(function (user) {
                userInvitingId = Number(user.invite.invitingId);
                userinvitedId = Number(user.invite.invitedId);
                if (
                    userInvitingId === invitingId
                    &&
                    userinvitedId === invitedId
                ) {
                    delete user.invite;
                    return true;
                }
            });

            return {...state, users};
        case CANCEL_INCOMING_INVITE:
            var invites = [...state.invites], inviteIndex = undefined;
            invites.some(function (invite, index) {
                if (invite.id === action.id) {
                    inviteIndex = index;
                    return true;
                }
            });
            if (inviteIndex != undefined) invites.splice(inviteIndex, 1);
            return {...state, invites};
        case ECHO_INVITE_HAS_COME:
            var invites = [...state.invites];
            invites.push(action.invite);
            return {...state, invites};
        case CLEAR_INVITES:
            return {...state, invites: []};

        case ECHO_CONSTRUCT_LEVEL:
            var level = action.level;
            if (level.user1.id == state.userId) level.creatorId = level.user1.id;
            return {
                ...state,
                level,
                requestPull: true,
                invites: [],
                users: []
            };

        case ECHO_PLAYER_MOVE:
            var level = {...state.level};
            level.move_player_id = action.userId;
            return {...state, requestPull: false, level};

        case ECHO_REDUCTION_MOVES:
            var level = {...state.level};
            level.countMoves--;
            return {...state, level};

        case ECHO_CLEAR_ZONE:
            return {...state, level: clearZone({...state.level}, action.userId, action.zoneIndex)};

        case ECHO_ADD_CHAT_MESSAGE:
            var level = {...state.level};
            level.messages.push(action.text);
            return {...state, level};

        case ECHO_GO_TO_ZONE:
            var level = setUserField({...state.level}, action.userId, 'zone_index', action.zoneIndex);
            level = openZone(level, action.zoneIndex);
            return {...state, level};

        case ECHO_USER_DAMAGE:
            var level = setUserField({...state.level}, action.userId, 'hp', action.hp);
            return {...state, level};

        case ECHO_MONSTER_DAMAGE:
            var level = {...state.level};
            let zone = getZone(level, action.zoneIndex);
            let monsters = zone.monsters;
            monsters.some(function (monster, i) {
                if (monster.id == action.monsterId) {
                    let monsterChanged = {...monster};
                    monsterChanged.hp = action.hp;
                    monsters[i] = monsterChanged;
                    if (monster.hp == 0) {
                        monsters.splice(i, 1);
                        level.countMonsters--;
                    }
                    return true;
                }
            });
            return {...state, level};

        case EQUIP_WEAPON:
            var level = {...state.level}, user = getUser(level, state.userId);
            user.weapon_id = action.id;

            return {
                ...state,
                level
            };
        case UNEQUIP_WEAPON:
            var level = {...state.level};
            level.user1.weapon_id = 0;

            return {
                ...state,
                level
            };

        case ECHO_BATTLE:
            return {...state, level: setUserField({...state.level}, action.userId, 'battle', action.battle)};

        case ECHO_END_GAME:
            var level = {...state.level}, textModal = {...state.textModal};

            textModal.title = action.title;
            textModal.text += action.text;

            return {...state, level: null, requestPull: false, textModal, users: action.users};

        case REQUEST_ERROR:
            alert('Упс! Запрос не прошёл. Повторите действие или перезагрузите страницу.');
            return {...state};

        case CHANGE_MODAL_TEXT:
            var textModal = {...state.textModal};
            textModal.title = action.title;
            textModal.text = action.text;
            return {...state, textModal};

        default:
            return state;
    }
}


