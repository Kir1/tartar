import {
    attackMonster
} from '../gameManager';
import ReactTooltip from 'react-tooltip';
import {bindEvents, getImageUrl, getMonsterIcon} from '../../helpers';
import React from 'react';
import {connect} from 'react-redux';

class Monster extends React.Component {
    constructor(props) {
        super(props);
        bindEvents(this, ['attack', 'rageAttack']);
    }

    attack() {
        this.props.attackMonster(this.props.monster.id);
    }

    rageAttack() {
        this.props.attackMonster(this.props.monster.id, true);
    }

    render() {
        let baseInfoId = 'monsterInfo' + this.props.monster.id,
            attackInfoId = 'attack' + this.props.monster.id,
            specAttackInfoId = 'rageAttack' + this.props.monster.id;

        return (
            <div className={"monster"}>
                <div className="main">
                    <img className="info" src={getMonsterIcon(this.props.monster.image)} data-tip
                         data-for={baseInfoId}/>
                    <div className="name">{this.props.monster.name}</div>
                    <div className="hp">Жизни: {this.props.monster.hp}/{this.props.monster.max_hp}</div>
                    <div className="damage">Урон: {this.props.monster.damage}</div>
                    <ReactTooltip id={baseInfoId} type='dark' className="box">
                        <img className="img" src={this.props.monster.image}/>
                    </ReactTooltip>
                </div>

                <div className="buttons">
                    <img className="attack" onClick={this.attack} src={getImageUrl('images/attack')} data-tip
                         data-for={attackInfoId}/>
                    <ReactTooltip id={attackInfoId} type='dark'>
                        Атака.
                    </ReactTooltip>

                    <img className="spec-attack" onClick={this.rageAttack} src={getImageUrl('images/attackRage')}
                         data-tip
                         data-for={specAttackInfoId}/>
                    <ReactTooltip id={specAttackInfoId} type='dark'>
                        Яростная атака. Наносит случайный урон от 0 до x, где x - это ваш двойной урон.
                    </ReactTooltip>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
    attackMonster: (id, rage) => dispatch(attackMonster(id, rage))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Monster)