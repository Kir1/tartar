import React from 'react';

export default class ZoneRaw extends React.PureComponent {
    render() {
        return (
            <div className="zone-raw">
                {this.props.children}
            </div>
        );
    }
}