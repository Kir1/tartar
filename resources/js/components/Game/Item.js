import {bindEvents} from '../../helpers';
import {equip} from '../gameManager';
import React from 'react';
import {connect} from 'react-redux';

class Item extends React.Component {
    constructor(props) {
        super(props);
        bindEvents(this, ['equip']);
    }

    equip() {
        this.props.equip(this.props.item.id);
    }

    render() {
        let weaponEquip = (this.props.userWeaponId == this.props.item.id),
            weaponClasses = '',
            weaponButtonClasses = '',
            weaponText = 'Экипировать',
            actionButton = this.equip;

        if (weaponEquip) {
            weaponButtonClasses = 'equip';
            weaponText = 'Экипировано';
            actionButton = undefined;
        }

        if (this.props.battle) {
            weaponClasses = 'battle';
            actionButton = undefined;
        }

        return (
            <div className={"weapon " + weaponClasses}>
                <img className="image" src={this.props.item.image}/>
                <div className="name">{this.props.item.name}</div>
                <div className="damage">Урон: {this.props.item.damage}</div>
                <div onClick={actionButton} className={"button" + ' ' + weaponButtonClasses}>{weaponText}</div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userWeaponId: state.game.level.user1.weapon_id,
    battle: state.game.level.user1.battle
})

const mapDispatchToProps = dispatch => ({
    equip: (id) => dispatch(equip(id))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Item)