import React from 'react';

export default class Empty extends React.PureComponent {
    render() {
        return (
            <div className="zone" style={{opacity: 0}}/>
        );
    }
}