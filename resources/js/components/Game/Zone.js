import {
    goToZone,
    figthZone
} from '../gameManager';
import ReactTooltip from 'react-tooltip';
import {bindEvents} from '../../helpers';
import React from 'react';
import {connect} from 'react-redux';

class Zone extends React.Component {
    constructor(props) {
        super(props);
        bindEvents(this, ['figth', 'move']);
    }

    figth() {
        this.props.figth(this.props.zone.id);
    }

    move() {
        this.props.move(this.props.zone.id);
    }

    render() {
        let name = '',
            infoWeapons = [],
            infoNotes = 0,
            infoMonsters = [],
            empty = true,
            user1InZone = this.props.user1ZoneIndex == this.props.zone.index,
            user2InZone = this.props.user2ZoneIndex == this.props.zone.index,
            statusClass = '',
            user1Figth = this.props.user1Figth,
            user2Figth = this.props.user2Figth,
            buttonText = 'Перейти',
            buttonFun = this.move
        ;

        if (user1Figth && this.props.user1Hp > 0) user1Figth = ' сражается!'; else user1Figth = '';
        if (user2Figth && this.props.user2Hp > 0) user2Figth = ' сражается!'; else user2Figth = '';

        if (
            (this.props.userId == this.props.user1Id && user1InZone)
            ||
            (this.props.userId == this.props.user2Id && user2InZone)
        ) {
            buttonFun = null;
            buttonText = null;
        }

        if (this.props.zone.monsters.length > 0) {
            empty = false;
            this.props.zone.monsters.forEach(element => infoMonsters.push(element.name + '. '));

            if (
                (this.props.userId == this.props.user1Id && user1Figth == false && user1InZone)
                ||
                (this.props.userId == this.props.user2Id && user2Figth == false && user2InZone)
            ) {
                buttonText = 'В бой!';
                buttonFun = this.figth;
            }
        }
        else if (this.props.zone.weapons.length > 0) {
            empty = false;
            this.props.zone.weapons.forEach(element => infoWeapons.push(element.name + '. '));
        }

        if (this.props.zone.notes > 0) {
            empty = false;
            infoNotes = this.props.zone.notes;
        }

        if (user1InZone || user2InZone) empty = false;

        name = 'zone' + this.props.zone.index;

        if (user1InZone) statusClass = ' player1';
        else if (user2InZone) statusClass = ' player2';
        else if (this.props.zone.open) statusClass = ' open';

        return (
            <div className={"zone " + statusClass} data-tip data-for={name}>
                {this.props.zone.index}

                <ReactTooltip id={name} type='dark'>
                    {this.props.zone.open ?
                        <>
                        {infoWeapons.length > 0 && <div>Оружие: <br></br> {infoWeapons}</div>}
                        {infoMonsters.length > 0 && <div className="tooltipUp">Монстры: <br></br> {infoMonsters}</div>}
                        {infoNotes > 0 && <div className="tooltipUp">Записки: {infoNotes}</div>}
                        {user1InZone &&
                        <div className="tooltipUp name-player1">Игрок {this.props.user1Name}{user1Figth}</div>
                        }
                        {user2InZone &&
                        <div className="tooltipUp name-player2">Игрок {this.props.user2Name}{user2Figth}</div>
                        }
                        {empty > 0 && <div>Тут ничего нет, ничего.</div>}
                        </>
                        :
                        <div className="tooltipUp">Не исследовано.</div>
                    }

                    <div className="tooltipDown"></div>
                </ReactTooltip>

                {buttonFun &&
                <div className="button" onClick={buttonFun}>
                    {buttonText}
                </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userId: state.game.userId,

    user1Id: state.game.level.user1.id,
    user1Hp: state.game.level.user1.hp,
    user1ZoneIndex: state.game.level.user1.zone_index,
    user1Name: state.game.level.user1.name,
    user1Figth: state.game.level.user1.battle,

    user2Id: state.game.level.user2.id,
    user2Hp: state.game.level.user2.hp,
    user2ZoneIndex: state.game.level.user2.zone_index,
    user2Name: state.game.level.user2.name,
    user2Figth: state.game.level.user2.battle,
})

const mapDispatchToProps = dispatch => ({
    move: (id) => dispatch(goToZone(id)),
    figth: (id) => dispatch(figthZone(id))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Zone)