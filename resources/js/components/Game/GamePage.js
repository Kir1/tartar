import Wrapper from '../Wrapper';
import Zone from '../Game/Zone';
import Monster from '../Game/Monster';
import Empty from '../Game/Empty';
import ZoneRaw from '../Game/ZoneRaw';
import {Redirect} from "react-router-dom";
import {
    exit,
    restart,
    checkUserCreator as checkUserCreatorState,
    getMonstersInFigth as getMonstersInFigthState
} from '../../components/gameManager';
import {bindEvents} from '../../helpers';
import React from 'react';
import {connect} from 'react-redux';

class GamePage extends React.Component {
    constructor(props) {
        super(props);
        bindEvents(this, ['exit', 'restart']);
    }

    exit() {
        this.props.exit();
    }

    restart() {
        this.props.restart();
    }

    render() {
        if (this.props.level == null) return <Redirect to="/"/>;

        let zones = [];

        this.props.level.map.map((raw, i) => {
            var zonesRaw = [];

            raw.map((zoneIndex, i2) => {
                    if (zoneIndex === '-')
                        zonesRaw.push(<Empty key={i2}/>);
                    else {
                        zonesRaw.push(<Zone key={i2} zone={this.props.level.zones[zoneIndex]}/>);
                    }
                }
            )

            zones.push(
                <ZoneRaw key={i}>{zonesRaw}</ZoneRaw>
            );
        });

        let monsters = [];
        let monstersObj = {monsters: []};
        this.props.getMonstersInFigth(monstersObj);
        if (monstersObj.monsters.length > 0) {
            monsters = monstersObj.monsters.map((monster, i) => (
                <Monster key={i} monster={monster}/>
            ))
        }

        return (
            <Wrapper>
                <div className="game-page">
                    <div className="zones">
                        {zones}
                    </div>
                    {
                        monsters.length > 0 &&
                        <div className="battle-field">
                            {monsters}
                        </div>
                    }
                    <div className="game-main-parametrs">
                        <div>Количество ходов: {this.props.level.countMoves}</div>
                        <div>Монстры: {this.props.level.countMonsters}</div>
                        <div>Записки: {this.props.level.countNotes}</div>
                    </div>
                    <div className="game-main-elements">
                        <div className="restart" onClick={this.restart}>Начать заново</div>
                        <div className="exit" onClick={this.exit}>Выйти</div>
                    </div>
                </div>
            </Wrapper>
        );
    }
}

const mapStateToProps = state => ({
    level: state.game.level
})

const mapDispatchToProps = dispatch => ({
    exit: () => dispatch(exit()),
    restart: () => dispatch(restart()),
    getMonstersInFigth: (obj) => dispatch(getMonstersInFigthState(obj))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GamePage)